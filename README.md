# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Es un Single Page Web Site
Usa iconos como fuentes : de boxicons y de iconics y de icofonts.
Usa "nav bar" para tener la barra de menus y sus efectos
Usa owl Carousel en la Pagina Product Details ( es un carousel mas Avanzado)
Usa Pills en la seccion de FAQs y Terminos de Servicios, incluyendo "collapse" y "accordion"
Usa popovers y tooltips en la seccion de Servicios ( click en los iconos o sobre ellos)
Usa "counterup.js" para la seccion de counters
usa "aos.js" ( Animate on scroll library) para animacion de imagenes
usa "isotope-layout.js" (Filter & sort magical layouts) para darle efectos de filtrado x nombre a los productos
en la seccion de "contacto" esta el modal para abrir la forma de "Subscribirse" ( tiene un pequeño issue)

* Configuration
index.html : pagoina principal
portfolio-details.html : pagina con detalles de productos
style.css : definicion de esilos en css
main.js : coddigo de JavaScript
Direccion /img:  imagenes usadas
directorio vendor: algunso otros js files y accusamus

* Dependencies
"@icon/icofont": "^1.0.1-alpha.1",
"aos": "^2.3.4",
"bootstrap": "^4.5.0",
"boxicons": "^2.0.5",
"isotope-layout": "^3.0.6",
"jquery": "^3.5.1",
"owl.carousel": "^2.3.4",
"popper.js": "^1.16.1",
"venobox": "^1.8.9"

* Database configuration
- Ninguna todavia

* How to run tests
- Usando lite-server-
- Ejecutando: npm run dev

* Deployment instructions
git clone https://Tunchito@bitbucket.org/Tunchito/jc-startup.git

### Contribution guidelines ###

* Writing tests

* Code review

* Code Issues
en la seccion de "contacto" esta el modal para abrir la forma de "Subscribirse" ( tiene un pequeño issue)
no puedo hacer que el boton de cerrar funione . Hay un problema con el modal-backdrop.
Fix: En inspect ( google chrome browser) si le doy hide elemnt, el modal esta ok y ya puede precionar el close para cerrarse.



* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
- JC

* Other community or team contact
